#ifndef LUNAV_TOP_H
#define LUNAV_TOP_H

#include "peripheral/Adc14.hpp"
#include "mkii/Button.hpp"
#include "mkii/Led.hpp"
#include "mkii/LightSensor.hpp"
#include "mkii/Timer.hpp"
#include "mkii/event/LedTimeout.hpp"

/**
 * @brief The top module acts as a container which manages and communicates
 * all the components.
 */
class Top {
 public:
	/**
	 * Obtain singleton instance of class.
	 */
	static Top* GetTop(void);

	void Init();

	/**
	 * Blink led i_u32BlinkCount times for a timer count i_u32TimerCount.
	 * This will cause a timer to count up to i_u32TimerCount for
	 * 2*i_u32BlinkCount times, toggling the led's output each time.
	 * @param i_u32BlinkCount Quantity of times the led blinks.
	 * @param i_u32TimerCount Timer Counter.
	 */
	void Blink(uint32_t i_u32BlinkCount, uint32_t i_u32TimerCount);

	/**
	 * Obtain Led object.
	 *
	 * @return mkii::Led* The led object.
	 */
	mkii::Led* GetLed(void);

	/**
	 * Obtain Button object.
	 *
	 * @return mkii::Button* The Button object.
	 */
	mkii::Button* GetButton(void);

	/**
	 * Obtain the Light Sensor object.
	 *
	 * @return mkii::LightSensor* The LightSensor object.
	 */
	mkii::LightSensor* GetLightSensor(void);

	/**
	 * Obtain Timer object.
	 */
	mkii::Timer* GetTimer(void);
	mkii::Timer* GetTimer2(void);

 private:
	/**
	 * Singleton instance of the class.
	 */
	static Top* m_pInstance;

	/**
	 * Led object represents the bulbs.
	 */
	mkii::Led* m_pLed;

	/**
	 * The user button to toggle state of lights.
	 */
	mkii::Button* m_pButton;

	/**
	 * The light sensor to be used.
	 */
	mkii::LightSensor* m_pLightSensor;

	/**
	 * Timer used to time the required intervals.
	 */
	mkii::Timer* m_pTimer;
	mkii::Timer* m_pTimer2;

	/**
	 * Class constructor.
	 */
	explicit Top();
};

#endif /* LUNAV_TOP_H */
