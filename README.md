# LUNAV

# Laboratorio #1: Luz de noche accionada por voz.

_Copia de documento provisto en el material del curso_

## Descripción

  Su empresa, Circuititos S.A., desea incursionar en el mundo de las luces nocturnas para niños, con el objetivo de proporcionar a miles de padres una solución para cuando sus niños se despiertan en medio de la noche, y estos todavía no alcanzan los interruptores para encender las luces.

  Circuititos S.A. desea posicionarse en el mercado con la primera luz de noche activada por voz. Usted, como Ingeniero encargado del proyecto, deberá realizar el primer prototipo del nuevo producto, el cual deberá cumplir con todos los requerimientos descritos en la tabla de requerimientos.

## Tabla de Requerimientos

  | Código  | Requerimiento                                                   | Descripción |
  | ------- | --------------------------------------------------------------- | ----------- |
  | SRS-001 |Número de luces a controlar                                      | El producto deberá ser capaz de controlar una, dos o tres lámparas LED, para proveer al mercado versiones de 5, 10 y 15W de potencia. |
  | SRS-002 |Confirmación de funcionamiento                                   | Una vez energizado el sistema, las lámparas deberán parpadear 3 veces para confirmar al usuario que la lámpara está lista para usarse. Luego de los tres parpadeos, la lámpara quedará en estado inicial. |
  | SRS-003 |Estado inicial de iluminación                                    | El producto deberá ser capaz de determinar el nivel inicial de luz al momento en que el sistema es energizado, para determinar si el estado inicial de la lámpara es encendido o apagado. |
  | SRS-004 |Tiempo de lámpara encendida                                      | El tiempo mínimo de encendido de la lámpara será de 30min, luego de los cuales la lámpara se apagará si no encuentra una condición de encendido. |
  | SRS-005 |Tiempo de lámpara apagada                                        | La lámpara se mantendrá apagada indefinidamente, hasta que no se encuentre una condición de encendido. |
  | SRS-006 |Condición de encendido                                           | Si la lámpara se encuentra apagada, el nivel de iluminación ambiente se encuentra por debajo del límite día/noche y el nivel de sonido se encuentra ~10% por encima del nivel de audio promedio de los últimos 5 segundos por 1 segundo, la lámpara deberá encenderse. |
  | SRS-007 |Botón de control                                                 | El usuario dispondrá de un botón de control, el cual alternará el estado de la lámpara, sin importar el nivel de iluminación. Si al presionar el botón se espera la lámpara encendida, el tiempo de encendido será de 30min, posterior al cual la lámpara se apagará y regresará al modo automático. |

## Getting started

- Get git submodules

  Run the following commands:
  ```bash
  git submodule init
  git submodule update
  cd mkii
  git submodule init
  git submodule update
  ```

- Import folder containing repository as a CCS Project.

- Build and load to board.


## Contributors

- Emilio Rojas Álvarez `@emilio93`

- Brian Morera Madriz `@BRJMM`
