# Bitácora de Reuniones

## 7-4-2019

Organización del proyecto

| Proyecto  | Capa                      | Descripción                    |
|-----------|---------------------------|--------------------------------|
| lunav     | Top                       | Específico a la aplicación     |
| mkii      | Aplicación de periféricos | Independiente de la aplicación |
| mkii      | Clases de periféricos     | Conexión con driverlib         |
| driverlib | Driverlib                 | Conexión con hardware          |
|           | Hardware                  | Tarjeta de desarrollo          |

