#include "Top.hpp"

peripheral::Adc14* g_pAdc14;
bool g_bIsSoundLevelHigh;
const uint16_t g_u16SampleSize = 105;
uint64_t g_u64Results[g_u16SampleSize];
void ADC14_Trigger(void);
void ADC14_IRQHandler(void);

const uint64_t LUNAV_SOUND_THRESHOLD = 8300;

/**
 * Initial blink duration.
 * 0.5 seconds toggle
 * f/divider * time = cycles
 * 48MHz/256 * 0.5 = 93750
 */
const uint32_t LUNAV_INIT_TIMER_COUNT_MILISECS = 500;
const uint32_t LUNAV_INIT_TIMER_COUNT =
    (uint32_t)(LUNAV_INIT_TIMER_COUNT_MILISECS * 48e3 / 256);

/**
 * Quantity of times the led is turn on during initialization.
 */
const uint32_t LUNAV_INIT_BLINK_COUNT = 3;

const float LUNAV_LUX_BOUNDARY = 500.0;

/**
 * Time the led stays turned on if it's not turned off using the button.
 * 5 seconds timeout
 * f/divider * time
 * 48MHz/256 * 5 = 937500
 */
const uint32_t LUNAV_LED_ON_DURATION_MILISECS = 5000;
const uint32_t LUNAV_LED_ON_DURATION =
    (uint32_t)(LUNAV_LED_ON_DURATION_MILISECS * 48e3 / 256);

int main(void) {
	/* Halting the Watchdog */
	MAP_WDT_A_holdTimer();

	// init results in 0
	for (int i = 0; i < g_u16SampleSize; i++) {
		g_u64Results[i] = 0;
	}

	Top* l_pTop = Top::GetTop();
	l_pTop->Init();

	/* Initialize top and create easy acces to submodules */
	mkii::Led* l_pLed = Top::GetTop()->GetLed();
	mkii::Button* l_pButton = Top::GetTop()->GetButton();
	mkii::Timer* l_pTimer = Top::GetTop()->GetTimer();
	mkii::Timer* l_pTimer2 = Top::GetTop()->GetTimer2();
	mkii::LightSensor* l_pLightSensor = Top::GetTop()->GetLightSensor();

	// set led off before blinking
	l_pLed->SetState(false);

	// start blinking event
	l_pLed->Blink(LUNAV_INIT_BLINK_COUNT, LUNAV_INIT_TIMER_COUNT, l_pTimer);

	// waits for blinking to be done
	while (l_pLed->IsBlinking()) {
		;
	}

	// obtain intial state
	l_pLightSensor->Init();
	bool l_bInitialState = l_pLightSensor->GetLux() < LUNAV_LUX_BOUNDARY;

	mkii::event::LedTimeout::GetLedTimeout(l_pLed, l_pTimer2,
	                                       LUNAV_LED_ON_DURATION);

	// set initial state.
	l_pLed->SetState(l_bInitialState, true);

	// enable button.
	l_pButton->TrackButtonPush(l_pLed);

	g_pAdc14 =
	    new peripheral::Adc14(peripheral::adc14::AnalogInputDevice::MICROPHONE);
	g_pAdc14->ConfigureDevice();
	g_pAdc14->EnableConversion();
	g_pAdc14->EnableAndRegisterInterrupt(ADC14_IRQHandler);
	g_pAdc14->TriggerSignalConversion();
	do {
	} while (1);
}

void ADC14_IRQHandler(void) {
	g_pAdc14->ClearInterruptFlag();

	uint64_t l_u64InterruptStatus = g_pAdc14->GetInterruptStatus();
	uint64_t l_u64Result = g_pAdc14->GetSimpleSampleModeResult();

	uint64_t l_u64Average = 0;
	// rearrange and get averag  samples for last n tests secs
	for (int i = g_u16SampleSize - 1; i > 0; i--) {
		l_u64Average = l_u64Average + g_u64Results[i];
		g_u64Results[i] = g_u64Results[i - 1];
	}
	g_u64Results[0] = l_u64Result;
	l_u64Average = l_u64Average + g_u64Results[0];
	l_u64Average = l_u64Average / g_u16SampleSize;
	if (l_u64Average > LUNAV_SOUND_THRESHOLD) {
		g_bIsSoundLevelHigh = true;
		if (!Top::GetTop()->GetLed()->GetState()) {
			Top::GetTop()->GetLightSensor()->Init();
			if (Top::GetTop()->GetLightSensor()->GetLux() < LUNAV_LUX_BOUNDARY) {
				Top::GetTop()->GetLed()->SetState(false);
				Top::GetTop()->GetLed()->SetState(true, true);
			}
		}
	} else {
		g_bIsSoundLevelHigh = false;
	}

	g_pAdc14->TriggerSignalConversion();
}
