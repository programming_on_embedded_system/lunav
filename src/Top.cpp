#include "Top.hpp"

Top* Top::m_pInstance = 0;

Top* Top::GetTop() {
	if (Top::m_pInstance == 0) {
		Top::m_pInstance = new Top();
	}
	return Top::m_pInstance;
}

void Top::Init() {
	MAP_PCM_setCoreVoltageLevel(PCM_VCORE1);

	/* Set 2 flash wait states for Flash bank 0 and 1*/
	MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
	MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

	/* Initializes Clock System */
	MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_48);
	MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);

	/* Enable Interrupts */
	MAP_Interrupt_enableMaster();
}

mkii::Timer* Top::GetTimer(void) { return this->m_pTimer; }
mkii::Timer* Top::GetTimer2(void) { return this->m_pTimer2; }

mkii::Led* Top::GetLed(void) { return this->m_pLed; }

mkii::Button* Top::GetButton(void) { return this->m_pButton; }

mkii::LightSensor* Top::GetLightSensor(void) { return this->m_pLightSensor; }

Top::Top(void) {
	this->m_pLed = new mkii::Led();
	this->m_pButton = new mkii::Button(mkii::button::ButtonId::S1);
	this->m_pTimer = mkii::Timer::GetTimer(mkii::timer::TimerTypes::TIMER_32_0);
	this->m_pTimer2 = mkii::Timer::GetTimer(mkii::timer::TimerTypes::TIMER_32_1);
	this->m_pLightSensor = new mkii::LightSensor();
}
